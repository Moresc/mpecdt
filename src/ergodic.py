import numpy as np
<<<<<<< HEAD
import pylab as pl
=======
import pylab
>>>>>>> mpecdt/master
from scipy import integrate
from data import getdata
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
<<<<<<< HEAD
#import statistics as st
=======
>>>>>>> mpecdt/master

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
<<<<<<< HEAD
    pl.title(title)
=======
    pylab.title(title)
>>>>>>> mpecdt/master

def ensemble(M,T):
    """
    Function to integrate an ensemble of trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """
    ens = 0.1*np.random.randn(M,3)

    for m in range(M):
        t = np.array([0.,T])
        data = integrate.odeint(vectorfield, 
                                y0=ens[m,:], t=t, args=(10.0,28.,8./3.),
                                mxstep=100000)

        ens[m,:] = data[1,:]

    return ens

def f(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f: - 1d- array of f values
    """

    return d[:,0]**2 + d[:,1]**2 + d[:,2]**2

<<<<<<< HEAD
def f1(d):
    #function to apply ergodic average, second example.
    #Inputs: X- array, first dimension time, secondimension component
    #outputs: f:-1d-array of f values
    
    return d[:, 0]**4+d[:, 1]**4+d[:, 2]**2


def SpatialAverage(M, T, f):
    """
    Returns the spatial averaqge
    """
    
    return np.average(f(ensemble(M, T)))

def TimeAverage(dt, T, f=f, args=(10., 28., 8./3.)):
    """
    Returns the time average for a function f given as argument
    """
    
    data= Orbit(dt, T, args)
    return np.average(f(data))

def Orbit(dt, T, args=(10.0, 28., 8./3.)):
    """
    Returns an orbit for random initial condition 
    with values from 0 to T and intervals of dt between each value
    """
    t=np.array([0., T])
    data= integrate.odeint(vectorfield, y0=0.1*np.random.randn(3), t=np.arange(0, T, dt), args=args, mxstep=100000)
    return data

def PlotTimeAver():

    """
    Plots the time average for 4 different initial conditions (generated randomly) we would like to appreciate the converge of the time average
    """
#Four vectors
    spaceaver1=[]
    spaceaver2=[]
    spaceaver3=[]
    spaceaver4=[]
    N=[]
    for i in range(0, 15):
#calculate the time average at time T=50+50*i for four different initial condition, we do this for i from 0 to 15
        a21=TimeAverage(0.1, 50+50*i)
        a22=TimeAverage(0.1, 50+50*i)
        a23=TimeAverage(0.1, 50+50*i)
        a24=TimeAverage(0.1, 50+50*i)
        ni=50+50*i
        spaceaver1.append(a21)
        spaceaver2.append(a22)
        spaceaver3.append(a23)
        spaceaver4.append(a24)
        N.append(ni)
#We plot the four vectors, we see that they all converge to the same result. Each value of the graph (for every of the T_n), is obtained from 4 independent initial conditions but we see that as we increase T, the distance between them is smaller. Moreover, since as T increases the difference in the value between T_n and T_{n-1} decreases, we conclude that the time average converges and independently of the initial condition.

    font = {'size'   : 14}
    pl.rc('font', **font)

    pl.clf()
    pl.plot(N, spaceaver1,    'k', label='in cond 1')
    pl.plot(N, spaceaver2,     'r', label='in cond 2')
    pl.plot(N, spaceaver3,     'b', label='in cond 3')
    pl.plot(N, spaceaver4,     'b', label='in cond 4')
    pl.legend(loc='best')
    pl.xlabel('N')
    pl.ylabel('$E(f)$')
    print("4 graphs from independently generated initial conditions converge to the same value, thus independence is demonstrated ")
    print("However, the rate of convergence is very slow, for T=700 it is still far from converging")

def VarSpaceAver():
    """
    Calculates the variance of the space average for T=50 and 4 different values of M 
    """
# a= array of number of particles
    a=[20, 100, 200, 400]
    for j in range(0,4):
#space average for M=a[i]. We do it 15 times independently to calculate the variance later.
        spaceaver=[]
        for i in range(15):
            a1=SpatialAverage(a[j], 50, f)
            spaceaver.append(a1)
#Variance
        mSA=np.average(spaceaver)
        nx=len(spaceaver)-1
        ax=(spaceaver-mSA)**2
        varSA=(nx+1)/nx*np.average(ax)
        print("Spatial variance for M=%d is %f" % (a[j], varSA))
    print("We observe that the variance decreases as we increase M although it is always very high")

def Exercise3a():
    """
    For different values of rho, sigma and omega, we plot the shape of the attractor by generating an initial condition and saving the values of the orbit until T=400 
"""
    shape = Orbit(0.1, 400, (10.0, 28., 8./3.))
    ensemble_plot(shape, 'standard')


    
    shape = Orbit(0.1, 400, (5, 28., 8./3.))
    ensemble_plot(shape,'sigma=5')

    

    shape = Orbit(0.1, 400, (15.0, 28., 8./3.))
    ensemble_plot(shape,'sigma=15')

    

    shape = Orbit(0.1, 400, (10.0, 33., 8./3.))
    ensemble_plot(shape,'rho=33')

    

    shape = Orbit(0.1, 400, (10.0, 23., 8./3.))
    ensemble_plot(shape,'rho=23')

    

    shape = Orbit(0.1, 400, (10.0, 28., 13./3.))
    ensemble_plot(shape,'beta=13/3')

    
    shape = Orbit(0.1, 200, (11.0, 28., 3./3.))
    ensemble_plot(shape,'beta=3/3')

    print("the atractor shape is sensitive to all of the changes if those are big enough. However, for beta and omega, we need smaller changes than for rho to change significantly the attractor shape")

def exprho(f, ind):
    """
    Plots the variation of the expectation (using time average) of f while varying rho, it also approximates the derivative of the expectation of f with respect to rho

"""
#Different values of rho
    rhochange=[]  
    a2=[]
    a1=np.arange(27, 29.1, 0.1) 
#Time average for this values
    for i in range(0, 21):
        a=TimeAverage(0.1, 2000, f, (10.0, a1[i], 8./3.))
        b=a1[i]
        rhochange.append(a)
        a2.append(b)
#Plot of the different values
    pl.clf()
    pl.plot(a2, rhochange,      'k--', label='sensitive respect rho')
    pl.legend(loc='best')
    pl.xlabel('rho')
    pl.ylabel('$E(f)$')
     #Derivative via finite difference
    der_rho=(rhochange[11]-rhochange[10])/0.1
    if ind==0:
        print("For f as given in the our program the changes in the expectation are linear with respect to rho, the expectation is very sensitive to rho changes")
        print("The derivative with respect to rho around 10 is %f. All points seem to have differentiable response" %(der_rho))
    else:
        print("For f=f1, the changes in the expectation are linear with respect to rho, the expectation is very sensitive to rho changes")
        print("The derivative with respect to rho around 10 is %f. All points seem to have differentiable response" %(der_rho))

    

   
def expsigma(f, ind):
    """
    Plots the variation of the expectation (using time average) of f while varying sigma, it also approximates the derivative of the expectation of f with respect to sigma

"""
#Different values of sigma
    sigmachange=[]  
    a2=[]
    a1=np.arange(9.5, 10.6, 0.1) 
#Time average for this values
    for i in range(0, 11):
        a=TimeAverage(0.1, 2000, f, (a1[i], 28, 8./3.))
        b=a1[i]
        sigmachange.append(a)
        a2.append(b)
#plot of the different values
    pl.clf()
    pl.plot(a2, sigmachange,      'k--', label='sensitive respect sigma')
    pl.legend(loc='best')
    pl.xlabel('sigma')
    pl.ylabel('$E(f)$')
        
   #Derivative using finite differences.
    der_sigma=(sigmachange[6]-sigmachange[5])/0.1
    if ind==0:
        print("For f as given in the our program the changes in the expectation are linear with respect to sigma, the expectation is not very sensitive to sigma changes")
        print("The derivative with respect to sigma around 10 is %f. Certain points seem to have non differentiable response" %(der_sigma))
    else:
        print("For f=f1, the changes in the expectation are linear with respect to sigma, the expectation is not very sensitive to sigma changes")
        print("The derivative with respect to sigma around 10 is %f. Certain points seem to have non differentiable response" %(der_sigma))


def expbeta(f, ind):
    """
    Plots the variation of the expectation (using time average) of f while varying beta, it also approximates the derivative of the expectation of f with respect to bet

"""
#different values of beta
    betachange=[]  
    a2=[]
    a1=np.arange(8./3.-0.5, 8./3.+0.6, 0.1) 
#Time average for this values
    for i in range(0, 11):
        a=TimeAverage(0.02, 2000, f, (10, 28, a1[i]))
        b=a1[i]
        betachange.append(a)
        a2.append(b)
#plot of the different values
    pl.clf()
    pl.plot(a2, betachange,      'k--', label='sensitive respect beta')
    pl.legend(loc='best')
    pl.xlabel('beta')
    pl.ylabel('$E(f)$')
        
    #Derivative using finite differences
    der_beta=(betachange[6]-betachange[5])/0.1
    if ind==0:
        print("For f as given in the our program the changes in the expectation are linear with respect to beta, the expectation is not very sensitive to beta changes")
        print("The derivative with respect to sigma around 8/3 is %f. There are certain points for which the response seems not differentiable" %(der_beta))
    else:
        print("For f=f1, the changes in the expectation are linear, the expectation is very sensitive on beta changes")
        print("The derivative with respect to beta around 8/3 is %f, all points seem to have differentialbe reponse" %(der_beta))
    
if __name__ == '__main__':

    #Ens = ensemble(100, 0.5)
    #ensemble_plot(Ens,'T=0.5')
    #pl.savefig('trial.pdf')
    #pl.show()

#Exercise 2 Variance of space average    

    VarSpaceAver()

#Exercise 2 time average
    PlotTimeAver()

    pl.show()
        
#Exercise 3 a
    
    Exercise3a()   
    pl.show() 
    

#Exercise 3 b rho change for function f
    exprho(f, 0)
    pl.show()

# Exercise 3 b rho change for function f1
    exprho(f1, 1)
    pl.show()


#Exercise 3 b sigma change for function f    
    expsigma(f, 0)
    pl.show()

#Exercise 3 b sigma change for function f1
    expsigma(f1, 1)
    pl.show()    
    
#Exercise 3 b beta change for function f
    expbeta(f, 0)
    pl.show()

#Exercise 3 b beta change for function f1
    expbeta(f1, 1)
    pl.show()


=======
if __name__ == '__main__':

    Ens = ensemble(100, 0.5)
    ensemble_plot(Ens,'T=0.5')
    pylab.show()
>>>>>>> mpecdt/master
