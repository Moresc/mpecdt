import numpy as np
import pylab as pl
from scipy import integrate

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x (in this case the Lorenz).
    
    Inputs:
    x - the value of x
    t - the value of t
    sigma - a parameter
    beta - another parameter
    rho-another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])



def lmap(x):
    """
    
    inputs:
    x - a numpy array of values
    
    outputs:
    y - a numpy array containing the double well polynomial evaluated on x
    """
    t=np.arange(0., 1, .1)
    data=integrate.odeint(vectorfield, x, t=t, args=(10.0,28.,8./3.))
    return data[-1, :]

def obs():
    """
    Function to get the observation. It integrates forward the initial conditions
    up to time 1 and then a normal perturbation with mean 0 and stardard deviation
    sigma is added to them.
    inputs: none
    outputs: the observation
    
    """
    #observation standard deviation.
    sigma=0.2
    #initial conditions
    y0=[-0.587, -0.563, 16.87]
    #integration of the orbit
    t=np.arange(0., 1, .1)
    data=integrate.odeint(vectorfield, y0, t=t, args=(10.0,28.,8./3.))
    #return of the noisy observations
    return data[-1,:]+0.2*np.random.randn(3)

def myprior():
   """
   Function to draw a sample from a prior distribution given by the
   normal distribution with standard deviation alpha.
   inputs: none
   outputs: the sample
   """
   #standard deviation of the normal distribution
   alpha = 0.5
   y0 = alpha*np.random.randn(3)+[-0.5, -0.55, 17.0]

   return y0

def Phi(f,x,y):
   """
   Function to return Phi, assuming a normal distribution for 
   the observation noise, with mean 0 and standard deviation sigma.
   inputs:
   f - the function implementing the forward model
   x - a value of the model state x
   y - a value of the observed data
   """
   #standard deviation
   sigma = 0.5
   return 0.5*np.linalg.norm(f(x)-y)**2/sigma**2

def mcmc(f,prior_sampler,x0,yhat,N,beta):
   """
   Function to implement the MCMC pCN algorithm.
   inputs:
   f - the function implementing the forward model
   prior_sampler - a function that generates samples from the prior
   x0 - the initial condition for the Markov chain
   yhat - the observed data
   N - the number of samples in the chain
   beta - the pCN parameter

   outputs:
   xvals - the array of sample values
   avals - the array of acceptance probabilities
   """
   xvals = np.zeros([N+1, 3])
   xvals[0, :] = x0
   avals = np.zeros(N)
   for i in range(N):
        
        w=prior_sampler()
        v=np.sqrt(1-beta**2)*xvals[i, :]+beta*w
        avals[i]=np.minimum(1, np.exp(Phi(f, xvals[i, :], yhat)-Phi(f, v, yhat)))
        u=np.random.rand()
        if u<avals[i]:
            xvals[i+1,  :]=v;
        else:
            xvals[i+1, :]=xvals[i, :];

   return xvals,avals

if __name__ == '__main__':    
   yhat = obs()
   x0 = [0,0,0]
   #MCMC pCN algorith
   xvals,avals = mcmc(lmap, myprior ,x0 ,yhat, 10000, 0.01)
   #Cumulative average of the acceptance rate
   cumavals = np.cumsum(avals)/np.arange(1,avals.size + 1)
   irange=np.arange(1, 10000+1)
   #plot of the cumulative average.
   pl.clf()
   pl.plot(irange, cumavals,      'k--', label='cumulative mean of acceptance probabilities')
   pl.legend(loc='best')
   pl.xlabel('N')
   pl.ylabel('$a values$')
   pl.show() 
