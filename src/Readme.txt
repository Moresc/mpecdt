Dear Colin,

The two files you should look at are:

-"lorenz-mcmc.py"
-"ac_cumul.pdf"


"lorenz-mcmc.py" contains the code I have implemented for this tutorial. In it I have implemented the mcmc method to calculate the posterior probability in a function. This function returns the acceptance rate of the proposal values as well as the values that takes the chain.  Then in the code it is also calculated the cumulative mean of the acceptance rate. We were supposed to tune beta (one of the parameters for the mcmc) to get an acceptance rate of 0.25. However, the cumulative mean, converges to 0, no matter the beta choosen. Moreover, when we look at the last values obtained in the mcmc we get that they are really far from the real initial conditions (which is what we are trying to estimate). Hence, it seems clear that the mcmc fails to converge to the posterior distribution. Thus, I have stopped going further in the questions asked in the lab, since the answers would obviously be wrong. 

In "ac-cumul.pdf" you can find the plot of the cumulative average of the acceptance probability distribution where you can appraciate how it converges to 0.

Francesc
