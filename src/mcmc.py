import numpy as np
import pylab

def lmap(x):
   """
   Function to evaluate y = a*x + b for a = 2, b = 1

   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the double well polynomial evaluated on x
   """
   a = 2
   b = 1
   return a*b + 1

def myprior():
   """
   Function to draw a sample from a prior distribution given by the
   normal distribution with variance alpha.
   inputs: none
   outputs: the sample
   """
   alpha = 1.0
   return alpha**0.5*np.random.randn()

def Phi(f,x,y):
   """
   Function to return Phi, assuming a normal distribution for 
   the observation noise, with mean 0 and variance sigma.
   inputs:
   f - the function implementing the forward model
   x - a value of the model state x
   y - a value of the observed data
   """
   sigma = 1.0e-1
   return (f(x)-y)**2/sigma**2

def mcmc(f,prior_sampler,x0,yhat,N,beta):
   """
   Function to implement the MCMC pCN algorithm.
   inputs:
   f - the function implementing the forward model
   prior_sampler - a function that generates samples from the prior
   x0 - the initial condition for the Markov chain
   yhat - the observed data
   N - the number of samples in the chain
   beta - the pCN parameter

   outputs:
   xvals - the array of sample values
   avals - the array of acceptance probabilities
   """
   xvals = np.zeros(N+1)
   xvals[0] = x0
   avals = np.zeros(N)

   for i in range(N):
      raise(NotImplementedError)

   return xvals,avals

if __name__ == '__main__':    
   yhat = 0.5
   x0 = 0.
   xvals,avals = mcmc(lmap,myprior,x0,yhat,1000,0.5)
