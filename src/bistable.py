import pylab
import numpy as np

def Vprime_bistable(x):
    """
    Function to compute the derivative of the bistable potential V'. If the input is a 
    numpy array, this returns a numpy array of all of the V'(X) VALUES
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array of V'(x) values.
    """
    return x**3-x

def V_bistable(x):
    """
    Function to compute the derivative of the bistable potential V. If the input is
    a numpy array, this returns a numpy array of all of the V(x) values.
    inputs:
    x - a numpy array of values 
    
    outputs:
    y - a numpy array of V(x) values.
    """    
    
    
    return (x**4)/4 - (x**2)/2

def Inv_meas(x, V, sigma):
    """
    function to compute the values of the invariant measure (rho(x)) for certain x inputs.
    If the input is a numpy array, this returns a numpy array of all of rho(x) values.
    input:
    x - a numpy array of values
    V - a python function that recieves a numpy array a return the potential evaluated on this array
    sigma - the value of sigma in the sde

    output:
    y - a numpy array of the evalutation of the invariant measure for the given V and sigma)
    """
    return 0.1*np.exp((-2./sigma**2)*V(x))

def square(x):
   """
   Function to compute the square of the input. If the input is
   a numpy array, this returns a numpy array of all of the squared values.
   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the square of all of the values
   """
   return x**2

def identity(x):
    """
    Function to compute the identity of the input. If the input is a numpy
    array, this returns a numpy array of all this values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing all of the values
    """

    return x

def brownian_dynamics(x,Vp,sigma,dt,T,tdump):
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW

        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
    return np.array(xvals), np.array(tvals)

def montecarlo(f, x):
    """
    Function to compute the Monte Carlo estimate of the expectation
    E[f(X)], with N samples

    inputs:
    f - a Python function that applies a chosen mathematical function to
    each entry in a numpy array
    x - A numpy array of the solution of our SDE at different times 
    outputs:
    y - a numpy array of the estimate of the expectation for every time we have value of 
    the solution of the SDE
    """
    fx = f(x)
    theta = np.cumsum(fx)/np.arange(1,fx.size + 1)
    return theta

if __name__ == '__main__':
    dt = 0.1
    sigma =0.5
    T = 100.0


#ex 2
    
#Matrix to keep the values of 2000 simulations up to time T
    hist_toplot = np.zeros((2000,T/dt + 1))
#Simulations
    for i in range(2000):
        x, t=brownian_dynamics(1, Vprime_bistable, sigma, dt, T, dt)
    
        hist_toplot[i, :]=x
#Code to plot the histrogram against the invariant measures
    fighist=pylab.figure()
    arange=[611, 612, 613, 614, 615, 616]
    a=[1, 5, 10, 40, 60, 100]
    for i in range(6):
        plots=fighist.add_subplot(arange[i])
        plots.hist(hist_toplot[:, a[i]*10], bins =100, normed=True, label="T=%d"%a[i])
        pylab.plot(np.arange(-1.5,1.5,0.1), Inv_meas(np.arange(-1.5,1.5,0.1),V_bistable,sigma))
        pylab.legend(loc="best")
#Conclusions
    print("Around t=1, most of the values are still acomulated at the right hand side of the potential, however it balances with time and at T=40 the values are more or less distributed following the shape of the invariant measures. From T=60 until T=100 the shape doesn't change much anymore. We see that the rate of convergence is quick. We know from theoretical results that we have exponential convergence to the invariant measure for this SDE.")  
    pylab.show()  

    
#ex 3

#Run of one simulation up to time 10000
    x, t=brownian_dynamics(1, Vprime_bistable, sigma, dt, 100000, dt)
#Computations using monte carlo of E(x) and E(x^2)
    theta = montecarlo(identity, x)
    theta1 =montecarlo(square, x)
    print("We see that the value of the E(x) converges to 0   and the value of E(x^2) converges to %f. From the plots, boths convergences are clear. It is hard to assest the convergence rate from the plot in the normal scale. However when we do the loglog plot, there seems to be certain linearity in the plots. Hence it seems that the convergence rate of the plot would be polynomial."%(theta1[theta1.size-1]))

#Plot of the values in linear scale
    pylab.figure(2)
    pylab.plot(np.arange(1,theta.size +1), theta)
    pylab.title('Identity') 

    pylab.figure(3)
    pylab.plot(np.arange(1, theta1.size +1), theta1)
    pylab.title('Square')
#Plot of the values in loglog scale
    pylab.figure(4)
    pylab.loglog(np.arange(1,theta.size +1), abs(theta))
    pylab.title('Identity loglog')
    
    pylab.figure(5)
    pylab.loglog(np.arange(1, theta1.size +1), abs(theta1-theta1[theta1.size-1]))
    pylab.title('Square loglog')

    pylab.show()
