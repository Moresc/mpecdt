import numpy as np
import pylab as pl

"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return x**2
def identity(x):
    """
    Function to compute the identity of the input. If the input
    is a numpy array, this returns a numpy array of all of the 
    identity values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the identity of all of the values
    """
    return x

def f(x):
    """
    Function to compute f(x) of the input x for f given in the second exercise. If the 
    input if a numpy array, this returns a numpy array of f(x_i) for all of x_i values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array of f(x) 
    """
    return np.exp(-10*x)*np.cos(x)

def normal(N):
    """
    Function to return a numpy array containing N samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)

def cluster_pdf(x):
    """
    Function to evaluate the PDF for the cluster distribution. If the input 
    is a numpy array, this returns a numpy array with the PDF evaluated at
    each of the values in the array.
    
    inputs:
    x - a numpy array of input values

    outputs:
    rho - a numpy array of rho(x)
    """

    Y=np.zeros(x.shape)
    Y[x>0]=(10*np.exp(-10*x))/(1-np.exp(10))
    Y[x>1]=0
    return Y

def cluster(N):
    """
    Function to return a numpy array containing N samples from the
    cluster distribution.
    
    inputs:
    N - number of samples

    outputs:
    X- the samples
    """
    u = np.random.rand(N)
    return -0.1*np.log(1 - u + u*np.exp(-10))

def normal_pdf(x):
    """
    Function to evaluate the PDF for the normal distribution (the
    normalisation coefficient is ignored). If the input is a numpy
    array, this returns a numpy array with the PDF evaluated at each
    of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    return np.exp(-x**2/2)

def uniform_pdf(x):
    """
    Function to evaluate the PDF for the standard uniform
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    y = np.ones(x.shape)
    y[x<0] = 0.0
    y[x>1] = 0.0
    return y

def importance(f, Y, rho, rhoprime, N):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    """
    Y = Y(N)
    w=rho(Y)/rhoprime(Y)
    C=np.sum(w)
    theta1=f(Y)*w
    theta=1/C*np.sum(theta1)

    return theta

if __name__ == '__main__':

#exercise 1 example using 100000 samples
    theta = importance(identity, normal,
                      uniform_pdf,normal_pdf,100000)
    print("theta = %f" %(theta))
#plot of the error in loglog scale as we increase the number of samples
    n_range=np.arange(100,100000,100)
    plot_theta = np.zeros(np.size(n_range))

    for i, n in enumerate(n_range):
        plot_theta[i] = abs(importance(identity, normal, uniform_pdf,normal_pdf, n)-1/2)


    pl.loglog(n_range, plot_theta, 'r', label = 'Uniform')
    pl.show()


#exercise 2 example using 100000 samples
    theta = importance(f,cluster, uniform_pdf, cluster_pdf,100000)
    print("theta = %f" %(theta))
#plot of the error in loglog scale as we increase the number of samples
    plot_theta2 = np.zeros(np.size(n_range))

    for i, n in enumerate(n_range):
        plot_theta2[i] = abs(importance(f, cluster, uniform_pdf, cluster_pdf, n)-10./101+(10*np.cos(1)-np.sin(1))/(101*np.exp(10)))

    pl.loglog(n_range, plot_theta2, 'g', label = 'cluster')
    pl.legend(loc = "best")
    pl.show()
