Computational Lab: Data and Uncertainty 1
=============================

This is the tutorial material for Computational Lab 1 in the Data and
Uncertainty core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpecdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

This Computational Lab is about the calculation of statistics for random
variables :math:`X`, of the form: :math:`\mathbb{E}[f(X)]`,
which satisfies 

.. math::
   \mathbb{E}[f(X)] = \int f(x) \pi(x) dx,

where :math:`\pi(x)` is the probability density function for 
:math:`X`.

These expectations can be approximated by numerical quadrature rules,
of the form

.. math::
   \mathbb{E}[f(X)] = \sum_{i=1}^n b_i f(c_i)

for quadrature points :math:`c_i`, and quadrature weights.

Important facts about quadrature rules:

- Different quadrature rules are required for different PDFs
  :math:`\pi(x)`.
- A quadrature rule is called p-th order if it is exact for all
  polynomials of degree p and below.
- If the function f is sufficiently smooth then the quadrature error
  decays exponentially as p tends to infinity.
- For a given number of quadrature points n and a given PDF, Gauss
  quadrature provides the most accurate quadrature rule, with order
  2n-1.

Exercise 1
--------

Consider the univariate normal distribution with mean zero and
variance 1,

.. math::
   \pi(x) = \frac{1}{\sqrt{2\pi}}e^{-\frac{x^2}{2}}.

The Gauss-Hermite quadrature rules give the highest order quadrature
for given number of quadrature points. For 2 quadrature points, we have

.. math::
   c_{1} = - 1, \quad c_2 = 1,\quad b_1 = b_2 = 1/2,

which is third-order.

.. container:: tasks  

   The file `mpecdt/src/expectations.py` currently computes the
   expectation of the constant function :math:`x=1`, using the
   Gauss-Legendre quadrature rule corresponding to the uniform
   distribution on :math:`[0,1]`. Modify this file to implement the
   third-order Gauss-Hermite quadrature rule given above.

.. container:: tasks  

   Verify that the rule is indeed exact for some chosen degree 4
   polynomial (you can compute the expectations exactly by repeated
   application of the integration by parts rule).

Exercise 2
----------

We will investigate what happens when we use a quadrature rule for one
PDF :math:`\pi_1(x)` for an expectation with respect to a second PDF
:math:`\pi_2(x)`. In this exercise, we take :math:`\pi_1(x)` to be the
PDF for the normal distribution with mean 0 and variance 1, for which
the Gauss-Hermite quadrature rules can be used. We then take
:math:`\pi_2(x)` to be the PDF for the normal distribution with mean 1
and variance 2.

By computing the expectation of :math:`f(x)=1` with respect to
:math:`\pi_2`, it can be shown that the Gauss-Hermite quadrature is
not exact, and hence is not even 0th order accurate for :math:`\pi_2`.

.. container:: tasks  

   Demonstrate using your code that this is indeed the case.

Exercise 3
----------

A better solution is to apply a change of variables formula, i.e.
to find a function :math:`g(x)` such that

.. math::
   \int f(x)\pi_2 dx = \int g(x) \pi_1 dx,

and to then use the Gauss-Hermite quadrature rule to integrate the
second integral. 

.. container:: tasks  

   Implement this using a linear change of variables
   :math:`y = ax + b` by making a new set of quadrature weights and
   quadrature points, and verify that the new quadrature rule for
   :math:`\pi_2` is still third order (this is because the linear
   transformation preserves polynomial degree).

The problem with Gauss quadrature is that the number of required
quadrature points explodes as the number of dimensions in the domain
of integration increases.

We shall investigate some methods of circumventing this explosion in
complexity. One is Monte Carlo methods, another is Laplace's method.
A third approach, that we won't consider here, is called sparse grid
quadrature, which is based on Analysis of Variance (ANOVA).

Exercise 4
----------

In Monte Carlo methods, instead of having set values, the quadrature
points are taken as random variables. In the most basic Monte Carlo
method, we take :math:`n` independent and identically distributed
random variables :math:`(X_1,\ldots,X_n)`, each with the same
distribution as :math:`X`, as the quadrature points, and the
quadrature weights are all set to :math:`1/n`. This means that the
approximation of the expectation is itself a random variable, which we
call an estimator. The error in the approximation is also a random
variable; for reasonable assumptions about the function :math:`f` and
the probability measure for :math:`X`, the expectation of the error is
proportional to :math:`1/n`. This convergence rate is independent of
the dimension of :math:`X`.

.. container:: tasks  
	       
   For the normal distribution with mean 0 and variance 1, write a
   function to generate :math:`n` Monte Carlo quadrature points. Choose a
   degree 4 polynomial :math:`f` (use the one you chose for previous
   exercises) and compute the error in the estimator for various
   :math:`n`. Plot the error on a log-log graph and verify the
   convergence rate. (You can repeat the calculation several times for
   each :math:`n` to compute the average error.)

Exercise 5
----------

Laplace's method is another efficient approximation method for
expectation values in high dimensions. There are two key assumptions
behind Laplace's method. First, there is a function :math:`g` such
that 

.. math::
   f(x)\pi(x) = e^{-g(x)},

and second, :math:`g` has a unique global minimum denoted by :math:`x_0`. In that
case we may expand :math:`g` about :math:`x_0` to obtain

.. math::
   \begin{align}
   g(x) &= g(x_0) + \underbrace{g'(x_0)}_{=0} (x-x_0) + \frac{1}{2} g''(x_0)(x-x_0)^2 +
   \cdots\\
   &= g(x_0) + \frac{1}{2} g''(x_0)(x-x_0)^2 + \cdots,
   \end{align}

where we have assumed :math:`x \in \mathbb{R}` for notational simplicity,
and where primes denote differentiation with respect to :math:`x`. Since
:math:`x_0` is the unique minimum, we have :math:`g''(x_0) >0`. Substituting this
expansion into :math:`\mathbb{E}[f(X)]` gives

.. math::
   \mathbb{E}[f(X)] \approx e^{-g(x_0)} \int_\mathbb{R} e^{-g''(x_0)(x-x_0)^2/2}
   {\rm d} x.

Then, making use of the fact that

.. math::
   \int_\mathbb{R} e^{-g''(x_0)(x-x_0)^2/2}
   {\rm d} x = \sqrt{\frac{2\pi}{g''(x_0)}},

we finally obtain the approximation

.. math::
   \mathbb{E}[f(X)] \approx e^{-g(x_0)}  \sqrt{\frac{2\pi}{g''(x_0)}} .

This approximation becomes very useful in higher dimensions, since the
complexity only increases linearly with dimension. It is valid
provided :math:`g` quickly increases away from :math:`x_0`. In this
case we may assume that significant contributions to the expectation
value :math:`\mathbb{E}[f(X)]` are from regions near :math:`x_0`,
where the Taylor expansion of :math:`g` can be applied. 

On a more
formal level the following asymptotic result holds.

Assume that :math:`\tilde{g}:[a,b]\to \mathbb{R}` is twice
differentiable and has a unique global minimum at :math:`x_0 \in [a,b]`. 
Then

.. math::
   \lim_{\varepsilon \to 0} \frac{\int_a^b e^{-\tilde g(x)/\varepsilon}{\rm
   d}x}{e^{-\tilde g(x_0)/\varepsilon} \sqrt{\frac{2\pi
   \varepsilon}{\tilde g''(x_0)}}} = 1.

(We formally have :math:`g(x) = \tilde g(x)/\varepsilon` in our description
of Laplace's method).

In this exercise, we consider the approximation of

.. math::
   \mathbb{E}[\cos(X)] = \frac{1}{\sqrt{2\pi \varepsilon}} 
   \int_{\mathbb{R}} \cos(x) e^{-x^2/(2\varepsilon)} {\rm d}x

by Laplace's method, where :math:`X` is a Gaussian with mean zero and 
variance :math:`\varepsilon>0`. We first rewrite the integral as

.. math::
   \begin{align}
   \mathbb{E}[\cos(X)] &= \frac{1}{\sqrt{2\pi \varepsilon}} 
   \int_{\mathbb{R}} (\cos(x)+1) e^{-x^2/(2\varepsilon)} {\rm d}x- \frac{1}{\sqrt{2\pi \varepsilon}} 
   \int_{\mathbb{R}}  e^{-x^2/(2\varepsilon)} {\rm d}x,\\
   &=  \frac{1}{\sqrt{2\pi \varepsilon}} 
   \int_{\mathbb{R}} (\cos(x)+1) e^{-x^2/(2\varepsilon)}{\rm d}x - 1,
   \end{align}

and apply Laplace's method to the remaining integral, 

.. math::
   \begin{align}
   I &= \frac{1}{\sqrt{2\pi \varepsilon}} 
   \int_{\mathbb{R}} (\cos(x)+1) e^{-x^2/(2\varepsilon)} {\rm d}x, \\
   &=  \frac{1}{\sqrt{2\pi \varepsilon}} \int_{\mathbb{R}} 
   e^{-x^2/(2\varepsilon) + \log (\cos(x)+1)} {\rm d}x.
   \end{align}

Hence, we can apply Laplace's method with

.. math::
   \tilde g(x) = \frac{x^2}{2} - \varepsilon \log (\cos(x)+1),

which has a unique global minimum :math:`x_0=0` for all :math:`\varepsilon>0` sufficiently small, since

.. math::
   \tilde g'(x) = x + \varepsilon \frac{\sin(x)}{1+\cos(x)} .

Hence :math:`\tilde g(x_0) = -\varepsilon \log (2)`, and
the second derivative satisfies

.. math::
   \tilde g''(x_0) = 1 + \frac{\varepsilon}{2} .

Finally we obtain the Laplace approximation

.. math::
   \mathbb{E}[\cos (X)] \approx \frac{e^{\log
    (2)}}{\sqrt{1+\varepsilon/2}} -1  = \frac{2}{\sqrt{1+\varepsilon/2}} -1,

for :math:`\varepsilon` sufficiently small.

.. container:: tasks  

   Plot the error in this approximation as a function of
   :math:`\varepsilon` and determine the order of approximation in
   :math:`\varepsilon` (the exact value is
   :math:`e^{-\varepsilon/2}`).

.. container:: tasks  

   Make a similar plot of the error using the third-order
   Gauss-Hermite quadrature rule. What is the order of approximation
   in :math:`\varepsilon`?  Why do we expect this?
